import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginStack from "../stacks/LoginStack";
import InicioStack from "../stacks/InicioStack";

export default function Navigation() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          component={LoginStack}
          name="loginStack"
          options={{ headerShown: false }}
        />
        <Stack.Screen
          component={InicioStack}
          name="inicioStack"
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
