import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, Image, StyleSheet } from "react-native";
import { Input, Button } from "react-native-elements";

export default function Practica() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/logo.jpg")}
        resizeMode="contain"
        style={styles.logo}
      />
      <Input
        style={styles.inputGeneral}
        placeholder="Usuario"
        leftIcon={{ type: "font-awesome", name: "user" }}
      />
      <Input
        style={styles.inputGeneral}
        placeholder="Password"
        leftIcon={{ type: "font-awesome", name: "lock" }}
      />
      <Button
        title="Login"
        buttonStyle={styles.botonLogin}
        containerStyle={styles.botonContenedor}
        onPress={() => navigation.navigate("calculadora")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 60,
  },
  inputGeneral: {
    fontSize: 14,
    paddingLeft: 10,
  },
  logo: {
    width: 150,
    height: 150,
  },
  botonLogin: {
    backgroundColor: "#007A38",
    borderRadius: 50,
  },
  botonContenedor: {
    width: 200,
  },
});
