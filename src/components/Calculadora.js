import React from "react";
import { View, Text, StyleSheet } from "react-native";

export default function Calculadora() {
  return (
    <View style={styles.contenedorPrincipal}>
      <View style={styles.contenedorFila}>
        <View style={[styles.contenedorNumero, { backgroundColor: "red" }]}>
          <Text>7</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "pink" }]}>
          <Text>8</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "blue" }]}>
          <Text>9</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>/</Text>
        </View>
      </View>
      <View style={styles.contenedorFila}>
        <View style={[styles.contenedorNumero, { backgroundColor: "skyblue" }]}>
          <Text>4</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "brown" }]}>
          <Text>5</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "coral" }]}>
          <Text>6</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>*</Text>
        </View>
      </View>
      <View style={styles.contenedorFila}>
        <View style={[styles.contenedorNumero, { backgroundColor: "#977400" }]}>
          <Text>1</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "coral" }]}>
          <Text>2</Text>
        </View>
        <View
          style={[styles.contenedorNumero, { backgroundColor: "darkgray" }]}
        >
          <Text>3</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>-</Text>
        </View>
      </View>
      <View style={styles.contenedorFila}>
        <View style={[styles.contenedorNumero, { backgroundColor: "bisque" }]}>
          <Text>0</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>.</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>+</Text>
        </View>
        <View style={[styles.contenedorNumero, { backgroundColor: "gray" }]}>
          <Text>=</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  contenedorPrincipal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contenedorFila: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  contenedorNumero: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    width: 50,
  },
});
