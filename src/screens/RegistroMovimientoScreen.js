import React, { useEffect, useState } from "react";
import { View, Text, AsyncStorage, StyleSheet, Alert } from "react-native";
import { Input, Button } from "react-native-elements";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";

export default function RegistroMovimientoScreen() {
  const navigation = useNavigation();

  const [usuario, setUsuario] = useState();
  const [tipo, setTipo] = useState();
  const [valor, setValor] = useState();
  const [observacion, setObservacion] = useState();

  useEffect(() => {
    recuperarDatosUsuario();
  }, []);

  const recuperarDatosUsuario = async () => {
    try {
      const usuarioData = await AsyncStorage.getItem("usuarioKey");
      setUsuario(JSON.parse(usuarioData));
    } catch (error) {
      console.log(error);
    }
  };

  const registrar = async () => {
    if (tipo === "GASTO") {
      Alert.alert("Erro de validacion", "No es gasto");
      return;
    }

    try {
      await axios.post(
        "http://www.scientialabs.club/api/v1/movimientos",
        {
          observacion: observacion,
          tipo: tipo,
          valor: valor,
          usuario: usuario.idUsuario,
        },
        {
          headers: {
            Authorization: `Bearer ${usuario.token}`,
          },
        }
      );
      navigation.navigate("inicioScreen", { cargar: true });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.contenedor}>
      <Text style={styles.titulo}>Registro de Movimiento</Text>
      <Input
        placeholder="Tipo de movimiento"
        onChangeText={(e) => setTipo(e)}
      />
      <Input placeholder="Valor" onChangeText={(e) => setValor(e)} />
      <Input
        placeholder="Observación"
        onChangeText={(e) => setObservacion(e)}
      />
      <Button
        title="Registrar"
        onPress={registrar}
        buttonStyle={styles.boton}
        containerStyle={styles.botonContainer}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  contenedor: {
    flex: 1,
    backgroundColor: "white",
    padding: 20,
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 16,
    marginBottom: 20,
    alignSelf: "center",
  },
  boton: {
    borderRadius: 50,
    backgroundColor: "#007A38",
  },
  botonContainer: {},
});
