import React, { useState } from "react";
import { View, Text, Image, StyleSheet, Alert } from "react-native";
import { Input, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";

export default function RegistroScreen() {
  const navigation = useNavigation();
  const [nombre, setNombre] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const crearCuenta = async () => {
    try {
      const resultado = await axios.post(
        "http://www.scientialabs.club/api/v1/registro",
        {
          clave: password,
          correo: email,
          nombre: nombre,
        }
      );
      if (resultado.status === 200) {
        navigation.navigate("loginScreen");
      } else {
        Alert.alert("Error en el proceso", "No se pudo crear el usuario");
      }
    } catch (error) {
      console.log(error);
      Alert.alert("Error en el proceso", "No se pudo crear el usuario");
    }
  };

  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/logo.jpg")}
        resizeMode="contain"
        style={styles.logo}
      />

      <Input
        style={styles.inputGeneral}
        placeholder="Nombre"
        onChangeText={(e) => setNombre(e)}
      />

      <Input
        style={styles.inputGeneral}
        placeholder="Usuario"
        onChangeText={(e) => setEmail(e)}
      />
      <Input
        style={styles.inputGeneral}
        placeholder="Password"
        onChangeText={(e) => setPassword(e)}
      />

      <Button
        title="Crear cuenta"
        buttonStyle={styles.botonLogin}
        containerStyle={styles.botonContenedor}
        onPress={crearCuenta}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  inputGeneral: {
    fontSize: 14,
    paddingLeft: 10,
  },
  logo: {
    width: 150,
    height: 150,
  },
  botonLogin: {
    backgroundColor: "#007A38",
    borderRadius: 50,
  },
  botonContenedor: {
    width: 200,
  },
  registrese: {
    fontWeight: "bold",
    color: "#007A38",
  },
  contenedorRegistrese: {
    paddingTop: 20,
  },
});
