import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { Input, Button } from "react-native-elements";

export default function RecuperarClaveScreen() {
  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/logo.jpg")}
        resizeMode="contain"
        style={styles.logo}
      />
      <Input style={styles.inputGeneral} placeholder="Correo" />
      <Button
        title="Recuperar Clave"
        buttonStyle={styles.botonLogin}
        containerStyle={styles.botonContenedor}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  inputGeneral: {
    fontSize: 14,
    paddingLeft: 10,
  },
  logo: {
    width: 150,
    height: 150,
  },
  botonLogin: {
    backgroundColor: "#007A38",
    borderRadius: 50,
  },
  botonContenedor: {
    width: 200,
  },
  registrese: {
    fontWeight: "bold",
    color: "#007A38",
  },
  contenedorRegistrese: {
    paddingTop: 20,
  },
  recuperarClave: {
    paddingTop: 20,
    color: "gray",
    fontWeight: "bold",
  },
});
