import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Alert,
  AsyncStorage,
} from "react-native";
import { Input, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import * as Location from "expo-location";
import Constants from "expo-constants";

export default function LoginScreen() {
  const navigation = useNavigation();

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  useEffect(() => {
    recuperarUbicacion();
  }, []);

  const recuperarUbicacion = async () => {
    try {
      const { status } = await Location.requestPermissionsAsync();
      if (status !== "granted") {
        console.log("El usuario no ha otorgado permisos");
      }
      const location = await Location.getCurrentPositionAsync({});
      console.log(location);
    } catch (error) {
      console.log(error);
    }
  };

  const login = async () => {
    console.log("Login");
    try {
      const respuesta = await axios.post(
        "http://www.scientialabs.club/api/v1/login",
        {
          clave: password,
          correo: email,
        }
      );

      if (respuesta.status === 200) {
        await guardarDatosUsuario(respuesta.data);
        navigation.navigate("inicioStack");
      }
    } catch (error) {
      console.log(error);
      Alert.alert("Error de autenticacion", "Usuario o claves incorrectas");
    }
  };

  const guardarDatosUsuario = async (data) => {
    try {
      await AsyncStorage.setItem("usuarioKey", JSON.stringify(data));
      console.log("Datos guardados");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/logo.jpg")}
        resizeMode="contain"
        style={styles.logo}
      />
      <Input
        style={styles.inputGeneral}
        placeholder="Usuario"
        leftIcon={{ type: "font-awesome", name: "user" }}
        onChangeText={(e) => setEmail(e)}
      />
      <Input
        style={styles.inputGeneral}
        placeholder="Password"
        leftIcon={{ type: "font-awesome", name: "lock" }}
        onChangeText={(e) => setPassword(e)}
        password={true}
        secureTextEntry={true}
      />
      <Button
        title="Login"
        buttonStyle={styles.botonLogin}
        containerStyle={styles.botonContenedor}
        onPress={login}
      />
      <View style={styles.contenedorRegistrese}>
        <Text>No tiene cuenta?</Text>
        <Text
          onPress={() => navigation.navigate("registroScreen")}
          style={styles.registrese}
        >
          {" "}
          Registrese
        </Text>
      </View>
      <Text
        style={styles.recuperarClave}
        onPress={() => navigation.navigate("recuperarClaveScreen")}
      >
        Recuperar Clave
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  inputGeneral: {
    fontSize: 14,
    paddingLeft: 10,
  },
  logo: {
    width: 150,
    height: 150,
  },
  botonLogin: {
    backgroundColor: "#007A38",
    borderRadius: 50,
  },
  botonContenedor: {
    width: 200,
  },
  registrese: {
    fontWeight: "bold",
    color: "#007A38",
  },
  contenedorRegistrese: {
    paddingTop: 20,
    alignItems: "center",
  },
  recuperarClave: {
    paddingTop: 20,
    color: "gray",
    fontWeight: "bold",
  },
});
