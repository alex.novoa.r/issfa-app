import Axios from "axios";
import React, { useEffect, useState } from "react";
import { View, Text, AsyncStorage, FlatList, StyleSheet } from "react-native";
import { Input, Button } from "react-native-elements";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";

export default function InicioScreen(props) {
  const navigation = useNavigation();
  const [movimientos, setMovimientos] = useState();

  useEffect(() => {
    recuperarDatosUsuario();
  }, [props.route.params]);

  const recuperarDatosUsuario = async () => {
    try {
      const usuarioData = await AsyncStorage.getItem("usuarioKey");
      recuperarMovimiento(JSON.parse(usuarioData));
    } catch (error) {
      console.log(error);
    }
  };

  const recuperarMovimiento = async (data) => {
    const respuesta = await axios.get(
      "http://www.scientialabs.club/api/v1/movimientos/" +
        data.idUsuario +
        "/usuario",
      {
        headers: {
          Authorization: `Bearer ${data.token}`,
        },
      }
    );
    setMovimientos(respuesta.data);
  };

  const renderItem = ({ item }) => (
    <View style={styles.contenedorItem}>
      <Text style={styles.tipo}>{item.tipo}</Text>
      <Text>{item.observacion}</Text>
      <Text>$ {item.valor}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <Text style={styles.titulo}>Mis Movimientos</Text>
      <FlatList
        data={movimientos}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
      <Button
        title="Agregar nuevo"
        buttonStyle={styles.boton}
        containerStyle={styles.botonContainer}
        onPress={() => navigation.navigate("registroMovimientoScreen")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  contenedorItem: {
    padding: 10,
    borderBottomColor: "gray",
    borderBottomWidth: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  tipo: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#007A38",
  },
  titulo: {
    fontWeight: "bold",
    alignSelf: "center",
    padding: 10,
    fontSize: 16,
  },
  boton: {
    borderRadius: 50,
    backgroundColor: "#007A38",
  },
  botonContainer: {
    padding: 20,
  },
});
