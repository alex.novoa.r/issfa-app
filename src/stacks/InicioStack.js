import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import InicioScreen from "../screens/InicioScreen";
import RegistroMovimientoScreen from "../screens/RegistroMovimientoScreen";

export default function InicioStack() {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        component={InicioScreen}
        name="inicioScreen"
        options={{ title: "Movimientos" }}
      />

      <Stack.Screen
        component={RegistroMovimientoScreen}
        name="registroMovimientoScreen"
        options={{ title: "Registrar Movimiento" }}
      />
    </Stack.Navigator>
  );
}
