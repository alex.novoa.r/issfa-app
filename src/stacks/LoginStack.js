import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../screens/LoginScreen";
import RegistroScreen from "../screens/RegistroScreen";
import RecuperarClaveScreen from "../screens/RecuperarClaveScreen";

export default function LoginStack() {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        component={LoginScreen}
        name="loginScreen"
        options={{ title: "Inicio de Sesión" }}
      />

      <Stack.Screen
        component={RegistroScreen}
        name="registroScreen"
        options={{ title: "Crear cuenta" }}
      />

      <Stack.Screen
        component={RecuperarClaveScreen}
        name="recuperarClaveScreen"
        options={{ title: "Recuperar Clave" }}
      />
    </Stack.Navigator>
  );
}
